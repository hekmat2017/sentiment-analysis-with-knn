#!/usr/bin/env python
#coding:utf-8
from numpy import *



# classify using KNN
def Knn_classify(newInput, newmarkedwords, dataset,datasetmarkedwords, labels,k):
    sumyy=[0 for j in range(len(dataset))]
    sumxx=[0 for j in range(len(dataset))]
    sumxy=[0 for j in range(len(dataset))]
    distance=[0 for j in range(len(dataset))]
    for i in range(len(dataset)-1):
        for j in range(len(datasetmarkedwords[i])-1):
            column=datasetmarkedwords[i][j]
            try:
                sumxy[i] +=(dataset[i][column].dot(newInput[column].T))
                sumxx[i] +=(dataset[i][column] ** 2).sum(1)
                sumyy[i] +=(newInput[column] ** 2).sum(1)
            except:
                 pass
        for j in range(len(newmarkedwords)-1):
            column=newmarkedwords[j]-1
            try:
                sumxy[i] +=(dataset[i][column].dot(newInput[column].T))
                sumxx[i] +=(dataset[i][column] ** 2).sum(1)
                sumyy[i] +=(newInput[column] ** 2).sum(1)
            except:
                pass
        try:    
            distance[i]=(sumxy[i]/np.sqrt(sumxx[i]))/np.sqrt(sumyy[i])
        except:
            pass
        
    ## step 2: sort the distance
    # argsort() returns the indices that would sort an array in a ascending order  
    sortedDistIndices = argsort(distance)
	    
    positive_label=0
    negative_label=0
    predict=0
    for i in range(k):  
        ## step 3: choose the min k distance  
	    voteLabel = labels[sortedDistIndices[i]]
	    ## step 4: count the times labels occur  
	    if(voteLabel==1):
	        positive_label+=1
	    else :
	        negative_label+=1
    ## step 5: the max voted class will return
    if(positive_label>=negative_label):
        predict=1
    else:
        predict=0
    return predict