========================================================
Data Mining：Sentiment Classification
Instructor: DR. Mohammad Akbari
Hekmat Beigverdi
15 Nov 2019
========================================================
The program is written in Python 2.6 with numpy package
========================================================
Note: These nine files should be in the same directory
    —preprocessing.py
    -knn_euclidean.py
    -knn_manhattan.py
    -knn_cosine.py
    —test.negative.txt
    -test.positive.txt
    —train.positive.txt
    -train.negative.txt
    -stopWords.txt
    -internetSlangs.txt




  
